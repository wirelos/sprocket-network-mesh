#ifndef __MESH_SPROCKET_CONFIG__
#define __MESH_SPROCKET_CONFIG__

#include <Arduino.h>
#include <ArduinoJson.h>
#include <FS.h>
#include "MeshConfig.h"
#include "SprocketConfig.h"
#include "JsonStruct.h"

#define JSON_stationMode "stationMode"
#define JSON_channel "channel"
#define JSON_meshPort "meshPort"
#define JSON_meshSSID "meshSSID"
#define JSON_meshPassword "meshPassword"
#define JSON_stationSSID "stationSSID"
#define JSON_stationPassword "stationPassword"
#define JSON_hostname "hostname"

struct MeshSprocketConfig : public JsonStruct {
    int stationMode;
    int channel;
    int meshPort;
    String meshSSID;
    String meshPassword;
    String stationSSID;
    String stationPassword;
    String hostname;
    uint16_t debugTypes;
    
    // ------------------------------------------------------------------------------------------
    void mapJsonObject(JsonObject& root) {
        root[JSON_stationMode] = stationMode;
        root[JSON_channel] = channel;
        root[JSON_meshPort] = meshPort;
        root[JSON_meshSSID] = meshSSID;
        root[JSON_meshPassword] = meshPassword;
        root[JSON_stationSSID] = stationSSID;
        root[JSON_stationPassword] = stationPassword;
        root[JSON_hostname] = hostname;
    }
    
    // Map a json object to this struct.
    void fromJsonObject(JsonObject& json) {
        stationMode = getIntAttrFromJson(json, JSON_stationMode);
        channel = getIntAttrFromJson(json, JSON_channel);
        meshPort = getIntAttrFromJson(json, JSON_meshPort);
        meshSSID = getAttrFromJson(json, JSON_meshSSID);
        meshPassword = getAttrFromJson(json, JSON_meshPassword);
        stationSSID = getAttrFromJson(json, JSON_stationSSID);
        stationPassword = getAttrFromJson(json, JSON_stationPassword);
        hostname = getAttrFromJson(json, JSON_hostname);
    };
};

#endif
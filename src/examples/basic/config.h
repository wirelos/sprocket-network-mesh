#ifndef __MESH_CONFIG__
#define __MESH_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip config
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       3000

// Mesh config
#define SPROCKET_MODE       0
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define MESH_PREFIX         "whateverYouLike"
#define MESH_PASSWORD       "somethingSneaky"
#define STATION_SSID        "Th1ngs4p"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "mesh-node"
#define MESH_DEBUG_TYPES    ERROR | STARTUP | CONNECTION
//ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE

// OTA config
#define OTA_PORT 8266
#define OTA_PASSWORD ""

// WebServer
#define WEB_CONTEXT_PATH "/"
#define WEB_DOC_ROOT "/www"
#define WEB_DEFAULT_FILE "index.html"

#endif
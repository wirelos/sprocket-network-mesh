#include "config.h"
#include "MeshApp.h"

MeshApp sprocket(
    {STARTUP_DELAY, SERIAL_BAUD_RATE},
    {SPROCKET_MODE, WIFI_CHANNEL,
     MESH_PORT, MESH_PREFIX, MESH_PASSWORD,
     STATION_SSID, STATION_PASSWORD, HOSTNAME,
     MESH_DEBUG_TYPES});

void setup()
{
    delay(3000);
    sprocket.activate();
}

void loop()
{
    sprocket.loop();
    yield();
}
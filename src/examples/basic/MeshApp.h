#ifndef __MESH_APP__
#define __MESH_APP__

#include <Sprocket.h>
#include <MeshNet.h>
#include <MeshNetworkPlugin.cpp>
#include <utils/print.h>

using namespace std;
using namespace std::placeholders;

class MeshApp : public Sprocket
{
  public:
    Task heartbeatTask;

    MeshApp(SprocketConfig cfg, MeshConfig meshCfg) : Sprocket(cfg)
    {
        addPlugin(new MeshNetworkPlugin(meshCfg));
        subscribe("device/heartbeat", bind(&MeshApp::messageHandler, this, _1));
    }

    Sprocket *activate(Scheduler *scheduler)
    {
        // add a task that sends stuff to the mesh
        heartbeatTask.set(TASK_SECOND * 5, TASK_FOREVER, bind(&MeshApp::heartbeat, this));
        addTask(heartbeatTask);
        PRINT_MSG(Serial,"MESH", "MeshApp activated");
        return this;
    }
    using Sprocket::activate;

    void messageHandler(String msg)
    {
        Serial.println(String("MeshApp: ") + msg);
    }

    void heartbeat()
    {
        SprocketMessage msg;
        msg.domain = "wirelos";
        msg.to = "broadcast";
        msg.payload = "alive";
        msg.topic = "device/heartbeat";
        msg.type = SprocketMessage::APP;
        String msgStr = msg.toJsonString();
        publish("mesh/broadcast", msgStr);
    }
};

#endif
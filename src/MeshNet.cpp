#include "MeshNet.h"

MeshNet::MeshNet(MeshConfig cfg) : Network() {
    config.stationMode = cfg.stationMode;
    config.channel = cfg.channel;
    config.meshPort = cfg.meshPort;
    config.meshSSID = cfg.meshSSID;
    config.meshPassword = cfg.meshPassword;
    config.stationSSID = cfg.stationSSID;
    config.stationPassword = cfg.stationPassword;
    config.hostname = cfg.hostname;
    config.debugTypes = cfg.debugTypes;
}

Network* MeshNet::init(){
    Serial.println("init mesh");
    config.fromFile("/config.json");
    //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
    mesh.setDebugMsgTypes( config.debugTypes );
    return this;
}

int MeshNet::connect(){
    mesh.init( config.meshSSID, config.meshPassword, scheduler, config.meshPort, WIFI_AP_STA, config.channel );
    mesh.onNewConnection(bind(&MeshNet::newConnectionCallback, this, _1));
    mesh.onChangedConnections(bind(&MeshNet::changedConnectionCallback, this));
    mesh.onNodeTimeAdjusted(bind(&MeshNet::nodeTimeAdjustedCallback, this, _1));
    if(config.stationMode){
        connectStation();
    }
    return 1;
}

int MeshNet::connectStation() {
        Serial.println("connect station");
        mesh.stationManual(config.stationSSID, config.stationPassword);
        mesh.setHostname(config.hostname.c_str());
        return 1;
}
void MeshNet::sendTo(uint32_t target, String msg){
    mesh.sendSingle(target, msg);
}

void MeshNet::broadcast(String msg, bool self){
    mesh.sendBroadcast(msg, self);
    
}
void MeshNet::update(){
    // only needed when no scheduler was passed to mesh.init
    mesh.update();
}

// example, not used, to be removed
void MeshNet::onReceive( std::function<void(uint32_t from, String &msg)> cb) {
    mesh.onReceive(cb);
}

void MeshNet::newConnectionCallback(uint32_t nodeId) {
    Serial.printf("--> New Connection, nodeId = %u\n", nodeId);
}

void MeshNet::changedConnectionCallback() {
    Serial.printf("--> Changed connections %s\n",mesh.subConnectionJson().c_str());
}

void MeshNet::nodeTimeAdjustedCallback(int32_t offset) {
    Serial.printf("--> Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}
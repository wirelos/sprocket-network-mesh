#ifndef __MESH_NETWORK_PLUGIN__
#define __MESH_NETWORK_PLUGIN__

#include "Plugin.h"
#include "TaskSchedulerDeclarations.h"
#include <Network.h>
#include <MeshNet.h>
#include "plugins/NetworkPlugin.cpp"
#include <SprocketMessage.h>

using namespace std;
using namespace std::placeholders;

class MeshNetworkPlugin : public NetworkPlugin
{
  public:
    MeshNetworkPlugin(MeshConfig cfg)
    {
        network = new MeshNet(cfg);
    }

    void activate(Scheduler *userScheduler)
    {
        network->onReceive(bind(&MeshNetworkPlugin::dispatch, this, _1, _2));
        subscribe("mesh/broadcast", bind(&MeshNetworkPlugin::broadcast, this, _1));
        // TODO mesh/sendTo
        NetworkPlugin::activate(userScheduler);
    }
    void broadcast(String msg)
    {
        network->broadcast(msg, true);
    }
    void dispatch(uint32_t from, String &msg)
    {
        SprocketMessage sMsg;
        sMsg.fromJsonString(msg);
        if (sMsg.valid)
        {
            sMsg.from = String(from);
            publish(sMsg.topic, sMsg.payload);
            return;
        }
        publish("mesh/message", msg);
    }
};

#endif
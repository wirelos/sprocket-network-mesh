#ifndef __MESH_MAN_PLUGIN__
#define __MESH_MAN_PLUGIN__

#include "TaskSchedulerDeclarations.h"
#include "ArduinoOTA.h"
#include "MeshNet.h"
#include "Plugin.h"
#include <ESPAsyncWebServer.h>
#include <MeshSprocketConfig.h>
#include <functional>

using namespace std;
using namespace std::placeholders;


class MeshManPlugin : public Plugin {
    private:
        MeshNet* net;
        AsyncWebServer* server;
    public:
        MeshManPlugin(MeshNet* mesh, AsyncWebServer* webServer){
            server = webServer;
            net = mesh;
        }
        void activate(Scheduler* userScheduler){
		    server->on("/mesh", HTTP_GET, std::bind(&MeshManPlugin::getMeshConnections, this, std::placeholders::_1));
            server->on("/mesh", HTTP_POST, std::bind(&MeshManPlugin::sendMsg, this, std::placeholders::_1));
            server->on("/mesh/nodeId", HTTP_GET, std::bind(&MeshManPlugin::getNodeId, this, std::placeholders::_1));
            server->on("/mesh/broadcast", HTTP_POST, std::bind(&MeshManPlugin::broadcast, this, std::placeholders::_1));

            subscribe("mesh/heartbeat", std::bind(&MeshManPlugin::gotHeartbeat, this, std::placeholders::_1));
        }
        void gotHeartbeat(String msg){
            Serial.println(String("MeshManPlugin / Heartbeat: ") + msg);
        }
        void getMeshConnections(AsyncWebServerRequest *request) {
            request->send(200, "text/plain", net->mesh.subConnectionJson());
        }
        void broadcast(AsyncWebServerRequest *request) {
            String msg = "";
            if(request->hasParam("msg", true)) {
                    msg = request->getParam("msg", true)->value();
            }
            msg = msg + "\0";
            net->mesh.sendBroadcast(msg);
            request->send(200, "text/plain", msg);
        }
        void sendMsg(AsyncWebServerRequest *request) {
            String msg = "";
            uint32_t to = 0;
            if(request->hasParam("msg", true)) {
                    msg = request->getParam("msg", true)->value();
            }
            if(request->hasParam("nodeId", true)) {
                    to = atoi(request->getParam("nodeId", true)->value().c_str());
            }
            msg = msg + "\0";
            net->mesh.sendSingle(to, msg);
            request->send(200, "text/plain", msg);
        }
        void getNodeId(AsyncWebServerRequest *request) {
            StaticJsonBuffer<200> jsonBuffer;
            JsonObject& root = jsonBuffer.createObject();
            root["nodeId"] = net->mesh.getNodeId();
            String jsonString;
            root.printTo(jsonString);
            request->send(200, "text/plain", jsonString);
        }
};

#endif
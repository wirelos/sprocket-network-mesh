#ifndef __MESHNET_H__
#define __MESHNET_H__

#ifdef ESP32
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif // ESP32

#include <painlessMesh.h>
#include <WiFiClient.h>
#include "Network.h"
#include "MeshConfig.h"
#include "MeshSprocketConfig.h"

using namespace std;
using namespace std::placeholders;

class MeshNet : public Network {
    public:
        painlessMesh mesh;
        MeshSprocketConfig config;
        MeshNet(MeshConfig cfg);
        Network* init();
        int connect();
        int connectStation();
        void configure(MeshSprocketConfig cfg);
        void update();
        void newConnectionCallback(uint32_t nodeId);
        void changedConnectionCallback();
        void nodeTimeAdjustedCallback(int32_t offset);
        void broadcast(String msg, bool self = false);
        void sendTo(uint32_t target, String msg);
        void onReceive(std::function<void(uint32_t from, String &msg)>);
        int isConnected(){
            return WiFi.status() == WL_CONNECTED;
        }
};

#endif
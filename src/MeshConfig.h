#ifndef __MESHCONFIG__
#define __MESHCONFIG__

// FIXME non-mesh config should have it's own struct
struct MeshConfig {
    int stationMode;
    int channel;
    int meshPort;
    const char* meshSSID;
    const char* meshPassword;
    const char* stationSSID;
    const char* stationPassword;
    const char* hostname;
    uint16_t debugTypes;
};


#endif